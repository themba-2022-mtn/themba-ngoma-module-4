import 'package:flutter/material.dart';
import 'package:flutter_theme/login.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Home',
      // theme: ThemeData(
      //  primarySwatch: Colors.blue,
      // ),

//-------------------------

      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(
          //primarySwatch: Colors.grey,
          primarySwatch: Colors.grey,
        ).copyWith(
          secondary: Colors.grey[350],
        ),

        //font
        fontFamily: 'Roboto',

        //textTheme: const TextTheme(bodyText2: TextStyle(color: Colors.purple)),
        // Define the default `TextTheme`. Use this to specify the default
        // text styling for headlines, titles, bodies of text, and more.
        textTheme: const TextTheme(
          headline1: TextStyle(fontSize: 72.0, fontWeight: FontWeight.bold),
          headline6: TextStyle(
              fontSize: 50.0,
              fontStyle: FontStyle.normal,
              fontFamily: 'Poppins',
              fontWeight: FontWeight.bold),
          bodyText2: TextStyle(fontSize: 14.0, fontFamily: 'Poppins'),
        ),
      ),

//-------------------------
      home: const Login(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return MaterialApp(
      theme: _buildShrineTheme(),
      home: Scaffold(
        appBar: AppBar(
          title: const Text(
            "Home",
          ),
        ),
        body: Builder(builder: (context) {
          return Stack(
            children: <Widget>[
              const Text(
                'You have pushed the button this many times:',
              ),
              Text(
                '$_counter',
                style: Theme.of(context).textTheme.headline4,
              ),
              //const MyNav(),
            ],
          );
        }),

        floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
        floatingActionButton: FloatingActionButton(
          onPressed: _incrementCounter,
          tooltip: 'Increment',
          child: const Icon(Icons.add),
          backgroundColor: Colors.yellow[600],
        ), // This trailing comma makes auto-formatting nicer for build methods.
      ),
    );
  }
}

//----------------------nav bar

class MyNav extends StatefulWidget {
  const MyNav({Key? key}) : super(key: key);

  @override
  State<MyNav> createState() => _MyNavState();
}

class _MyNavState extends State<MyNav> {
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    final colorScheme = Theme.of(context).colorScheme;
    final textTheme = Theme.of(context).textTheme;
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _currentIndex,
        backgroundColor: colorScheme.surface,
        selectedItemColor: colorScheme.onSurface,
        unselectedItemColor: colorScheme.onSecondary.withOpacity(.60),
        selectedLabelStyle: textTheme.caption,
        unselectedLabelStyle: textTheme.caption,
        onTap: (value) {
          // Respond to item press.
          setState(() => _currentIndex = value);
        },
        // ignore: prefer_const_literals_to_create_immutables
        items: const <BottomNavigationBarItem>[
          const BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            //title: Text('Favorites'),
          ),
          const BottomNavigationBarItem(
            // title: const Text('Music'),
            icon: const Icon(Icons.music_note),
          ),
          const BottomNavigationBarItem(
            //title: Text('Places'),
            icon: const Icon(Icons.location_on),
          ),
          const BottomNavigationBarItem(
            //title: const Text('News'),
            icon: const Icon(Icons.library_books),
          ),
        ],
      ),
    );
  }
}

//theme
ThemeData _buildShrineTheme() {
  final ThemeData base = ThemeData.light();
  return base.copyWith(
    colorScheme: _shrineColorScheme,
    textTheme: _buildShrineTextTheme(base.textTheme),
  );
}

TextTheme _buildShrineTextTheme(TextTheme base) {
  return base
      .copyWith(
          caption: base.caption?.copyWith(
            fontWeight: FontWeight.w400,
            fontSize: 14,
            letterSpacing: defaultLetterSpacing,
          ),
          button: base.button?.copyWith(
            fontWeight: FontWeight.w500,
            fontSize: 14,
            letterSpacing: defaultLetterSpacing,
          ),
          headline6: base.headline6?.copyWith())
      .apply(
        fontFamily: 'Rubik',
        displayColor: shrineBrown900,
        bodyColor: shrineErrorRed,
      );
}

const ColorScheme _shrineColorScheme = ColorScheme(
  primary: shrinePink100,
  primaryVariant: shrineBrown900,
  secondary: shrinePink50,
  secondaryVariant: shrineBrown900,
  surface: shrinePink50,
  background: shrinePink50,
  error: shrineErrorRed,
  onPrimary: shrineBrown900,
  onSecondary: shrineBrown900,
  onSurface: shrineBrown600,
  onBackground: shrineBrown900,
  onError: shrineSurfaceWhite,
  brightness: Brightness.light,
);

const Color shrinePink50 = Color(0xFF9E9E9E);
const Color shrinePink100 = Color(0xFF9E9E9E);
const Color shrinePink300 = Color(0xFFFBB8AC);
const Color shrinePink400 = Color(0xFFEAA4A4);

const Color shrineBrown900 = Color.fromRGBO(0, 0, 0, 1.0);
const Color shrineBrown600 = Color.fromRGBO(255, 255, 255, 1.0);

const Color shrineErrorRed = Color(0xFFC5032B);

const Color shrineSurfaceWhite = Color(0x000c5490);
const Color shrineBackgroundWhite = Colors.white;

// new colors

Color sGray10 = const Color.fromRGBO(128, 128, 128, 1.0);
Color c = const Color(0x00808080);
const defaultLetterSpacing = 0.03;
