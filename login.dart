import 'package:flutter/material.dart';

import 'main.dart';

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  State<Login> createState() => _LoginState();
}

class _LoginState extends State<Login> {
  @override
  Widget build(BuildContext context) {
    Widget form = Column(children: [
      Center(
        child: Padding(
          padding: const EdgeInsets.all(30),
          child: Text(
            'Hallo World',
            style: Theme.of(context).textTheme.headline6,
          ),
        ),
      ),
      Padding(
        padding: const EdgeInsetsDirectional.fromSTEB(24, 10, 24, 5),
        child: TextFormField(
          initialValue: 'UserName',
          decoration: const InputDecoration(
            labelText: 'UserName',
            border: OutlineInputBorder(),
          ),
        ),
      ),
      Padding(
        padding: const EdgeInsetsDirectional.fromSTEB(24, 10, 24, 5),
        child: TextFormField(
          initialValue: 'Password',
          decoration: const InputDecoration(
            labelText: 'Password',
            border: OutlineInputBorder(),
          ),
        ),
      ),
      Padding(
        padding: const EdgeInsets.all(20),
        child: ElevatedButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => const MyHomePage(
                    title: 'h',
                  ),
                ),
              );
            },
            child: const Text('LogIn')),
      )
    ]);

    return MaterialApp(
      theme: ThemeData(
        colorScheme: ColorScheme.fromSwatch(
          //primarySwatch: Colors.grey,
          primarySwatch: Colors.grey,
        ).copyWith(
          secondary: Colors.grey[350],
        ),
        //font
        fontFamily: 'Roboto',
      ),
      home: Scaffold(
        appBar: AppBar(
          title: const Text("Login"),
        ),
        body: Builder(builder: (context) {
          return Center(
              child: Column(
            children: [
              form,
            ],
          ));
        }),
      ),
    );
  }
}
